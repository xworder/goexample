package main

import (
	"context"
	"fmt"
)

type callbackLogic func(x, y int) (int, error)

//相当于一个函数类型还拥有自己的方法函数，方便扩展,函数式编程？
func (c callbackLogic) Double(num int) int {
	return num * 2
}

// type funcP func(x int, y bool) int
func Run1(
	ctx context.Context,
	x, y int,
	logic2run callbackLogic,
) error {
	ret, err := logic2run(x, y)
	fmt.Println(ret)
	d := logic2run.Double(ret)
	fmt.Println(d)
	return err
}

func Run2(
	ctx context.Context,
	x, y int,
	logic2run func(x, y int) (int, error),
) error {
	ret, err := logic2run(x, y)
	fmt.Println(ret)
	return err
}

func Logic1(x, y int) (int, error) {
	return x + y, nil
}

func main() {
	//参数和返回一致就认为是type func类型
	Run1(context.Background(), 1, 2, Logic1)

	Run2(context.Background(), 1, 2, Logic1)

	//参数和返回一致就认为是type func类型,使用匿名函数方式也可以
	Run1(context.Background(), 1, 2, func(x, y int) (int, error) {
		return x + y, nil
	})
	Run2(context.Background(), 1, 2, func(x, y int) (int, error) {
		return x + y, nil
	})

	//这里是将Logic1转为为callbackLogic类型,这样当这个类型有自定方法时，普通相同函数也可以调用，相当于直接扩展了普通函数
	newFunc := callbackLogic(Logic1)
	ret, _ := newFunc(1, 3)
	newFunc.Double(ret)
	fmt.Println(ret)
	ret2, _ := callbackLogic(Logic1)(1, 4)
	fmt.Println(ret2)
}

/*
Run函数的作用相当于就是一个封装，外部可以通过将函数作为参数传进去，相当于是注册进去，然后Run函数内部再去执行（回调）这个注册进去的函数
http handler 函数就是这种方式
由此扩展，其实自定义类型type 可以是所有的普通类型，比如int slice，func，struct等等,只要定义为type就可以去扩展子方法函数
*/
