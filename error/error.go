package error

import "fmt"

type UError struct {
	code int
	msg  string
}
/*
type error interface {
   Error() string
}
*/
//实现error接口
func (e UError) Error() string {
	return fmt.Sprintf("code:%d msg:%s", e.code, e.msg)
}

//工厂方法
func NewUError(code int, msg string) error {
	e := UError{
		code: code,
		msg:  msg,
	}
	return e
}

var ErrorA = UError{
	code: 111,
	msg:  "错误A",
}

//错误判断
//errors.As 会判断ErrorA是否是UError errors.Is 相当于严格判断，判断ErrorA 是否是ErrorA
