package main
 
import (
    "fmt"
    "reflect"
    "time"
)
 
var week time.Duration
 
func main() {
    t := time.Now()
 
    // 输出当前时间：
    fmt.Println(t, reflect.TypeOf(t)) // 2021-09-27 11:05:44.5965538 +0800 CST m=+0.018000701 time.Time
 
    // 格式化输出当前时间的两种方法
    ret1 := t.Format("2006-01-02 15:04:05")
    fmt.Println(ret1, reflect.TypeOf(ret1)) // 2021-09-27 11:05:44 string
    ret2 := fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
    fmt.Println(ret2, reflect.TypeOf(ret2)) // 2021-09-27 11:05:44 string
 
    // Add用法：计算一周后的日期
    week = 60 * 60 * 24 * 7 * 1e9  // 必须是nanosecond
    oneWeekLaterFromNow := t.Add(week)
    fmt.Println(oneWeekLaterFromNow, reflect.TypeOf(oneWeekLaterFromNow)) // 2021-10-04 11:08:24.107976 +0800 CST m=+604800.020852001 time.Time
 
    // 获取指定日期的0点时间
    ret3 := GetZeroTime(t)
    fmt.Println(ret3, reflect.TypeOf(ret3))
 
    // 获取指定日期所属月份的第一天0点时间
    ret4 := GetFirstDayOfMonth(t)
    fmt.Println(ret4, reflect.TypeOf(ret4))
 
    // 获取指定日期所属月份的最后一天0点时间
    ret5 := GetLastDayOfMonth(t)
    fmt.Println(ret5, reflect.TypeOf(ret5))
 
    // 获取当前周的周一
    ret6 := GetMondayOfCurrentWeek(t)
    fmt.Println(ret6, reflect.TypeOf(ret6))
 
    // 计算UTC时间
    ret7 := t.UTC()
    fmt.Println(ret7)
 
}
 
func GetZeroTime(t time.Time) time.Time {
    // 获取指定日期的0点时间
    return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
}
 
func GetFirstDayOfMonth(t time.Time) time.Time {
    // 获取指定日期所属月份的第一天0点时间
    d := t.AddDate(0, 0, -t.Day()+1)
    return GetZeroTime(d)
}
 
func GetLastDayOfMonth(t time.Time) time.Time {
    // 获取指定日期所属月份的最后一天0点时间
    return GetFirstDayOfMonth(t).AddDate(0, 1, -1)
}
 
func GetMondayOfCurrentWeek(t time.Time) time.Time {
    // 获取当前周的周一
    var offset int
    if t.Weekday() == time.Sunday {
        offset = 7
    } else {
        offset = int(t.Weekday())
    }
    return t.AddDate(0, 0, -offset+1)
}
