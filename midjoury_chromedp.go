package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"strings"
	"time"

	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
)

// 检查是否有9222端口，来判断是否运行在linux上
func checkChromePort() bool {
	addr := net.JoinHostPort("", "9222")
	conn, err := net.DialTimeout("tcp", addr, 1*time.Second)
	if err != nil {
		return false
	}
	defer conn.Close()
	return true
}

// ChromeCtx 使用一个实例
var ChromeCtx context.Context

func GetChromeCtx() {
	opts := []chromedp.ExecAllocatorOption{
		// chromedp.ExecPath(`C:\Program Files\Google\Chrome\Application\chrome.exe`),
		chromedp.UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3830.0 Safari/537.36"),
		// chromedp.WindowSize(1920, 1080),
		chromedp.NoFirstRun,
		chromedp.NoDefaultBrowserCheck,
		chromedp.Headless,
		chromedp.DisableGPU,
	}
	ChromeCtx, _ = chromedp.NewExecAllocator(context.Background(), opts...)
}

func main() {
	GetChromeCtx()
	MidJ()
}

func GetBuildID() string {
	timeCtx, cancel := chromedp.NewContext(ChromeCtx)
	defer cancel()

	var source string
	err := chromedp.Run(timeCtx,
		chromedp.Navigate("https://www.midjourney.com/home/"),
		// chromedp.Navigate("https://baidu.com"),
		// chromedp.WaitVisible(`#__NEXT_DATA__`, chromedp.ByID),
		// chromedp.OuterHTML(`html`, &source, chromedp.ByQuery),
		chromedp.InnerHTML(`#__NEXT_DATA__`, &source, chromedp.ByID),
		// chromedp.WaitVisible(`//ul[@class="news-list"]`),
		// chromedp.Location(&collectLink),
	)
	if err != nil {
		log.Println("run err", err.Error())
		return ""
	}

	// fmt.Println(source)

	r, _ := regexp.Compile("\"buildId\":\".*?\"")
	regRet := r.FindString(source)
	buildRet := strings.Split(regRet, ":")
	buildId := buildRet[1]
	buildId = strings.Trim(buildId, "\"")

	fmt.Println(buildId)

	return buildId
}

func MidJ() {
	buildId := GetBuildID()
	rurl := "https://www.midjourney.com/_next/data/" + buildId + "/showcase/recent.json"
	timeCtx, cancel := chromedp.NewContext(ChromeCtx) //new tab
	defer cancel()

	var source string
	err := chromedp.Run(timeCtx,
		chromedp.Navigate(rurl),
		// chromedp.WaitVisible(`#__NEXT_DATA__`, chromedp.ByID),
		chromedp.InnerHTML(`html`, &source, chromedp.ByQuery),
	)
	if err != nil {
		log.Println("run err2", err.Error())
		return
	}

	regex := regexp.MustCompile("<[^>]*>")
	resp := regex.ReplaceAllString(source, "")
	// fmt.Println(resp)
	// return

	type Recent struct {
		PageProps struct {
			Jobs []struct {
				ID         string   `json:"id"`
				Prompt     string   `json:"prompt"`
				ImagePaths []string `json:"image_paths"`
				ParentID   string   `json:"parent_id"`
			}
		}
	}

	var recent Recent
	err = json.Unmarshal([]byte(resp), &recent)
	if err != nil {
		panic(err)
	}
	file, err := os.OpenFile("../midshow/prompt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	// done := make(chan network.RequestID, 100)
	// defer close(done)

	// this will be used to capture the request id for matching network events
	requestIDs := make(map[network.RequestID]string)
	url2info := make(map[string]struct {
		ImagePath string
		Prompt    string
		ParentID  string
	})

	chromedp.ListenTarget(timeCtx, func(v interface{}) {
		switch ev := v.(type) {
		case *network.EventRequestWillBeSent:
			log.Printf("EventRequestWillBeSent: %v: %v", ev.RequestID, ev.Request.URL)
			requestIDs[ev.RequestID] = ev.Request.URL
			// if ev.Request.URL == urlstr {
			// 	requestID = ev.RequestID
			// }
		case *network.EventLoadingFinished:
			log.Printf("EventLoadingFinished: %v", ev.RequestID)
			// done <- ev.RequestID
			// if ev.RequestID == requestID {
			// 	// close(done)
			// }
			go func() {

				var buf []byte
				err = chromedp.Run(timeCtx, chromedp.ActionFunc(func(ctx context.Context) error {
					var err error
					buf, err = network.GetResponseBody(ev.RequestID).Do(ctx)
					if err != nil {
						log.Println("getresponsebody err", err)
					}
					return err
				}))
				if err != nil {
					log.Println("download run err", err)
					return
				}
				urlstr := requestIDs[ev.RequestID]
				imagepath := url2info[urlstr].ImagePath
				prompt := url2info[urlstr].Prompt
				parentid := url2info[urlstr].ParentID
				if err := os.WriteFile(imagepath, buf, 0644); err != nil {
					log.Println("download write err", err)
					return
				}

				file.WriteString(parentid + "_" + path.Base(urlstr) + "=>" + prompt + "\n")
				log.Println("done", imagepath)
			}()

		}
	})

	i := 0
	for _, v := range recent.PageProps.Jobs {
		// set the download url as the chromedp GitHub user avatar
		urlstr := v.ImagePaths[0]

		imagepath := "../midshow/" + v.ParentID + "_" + path.Base(urlstr)
		_, err := os.Stat(imagepath)
		if err == nil {
			continue
		}
		i++
		fmt.Println(i)

		// buf, err := download(timeCtx, urlstr)
		err = chromedp.Run(timeCtx,
			chromedp.Navigate(urlstr),
		)
		if err != nil {
			fmt.Println("download err", err)
			continue
		}
		url2info[urlstr] = struct {
			ImagePath string
			Prompt    string
			ParentID  string
		}{
			ImagePath: imagepath,
			Prompt:    v.Prompt,
			ParentID:  v.ParentID,
		}

	}

	time.Sleep(10 * time.Second)

}
